/**
 * Created by root on 24.03.16.
 */


class PriorityQueue {
    constructor() {
        this.inner = [];
    }

    *[Symbol.iterator]() {
        for(let i = 0;i<this.inner.length;i++){
            yield this.inner[i];
        }
    }
    popMax() {
        return this.inner.pop();
    }

    popMin(){
        return this.inner.splice(0,1)[0];
    }

    __add(index, value) {
        if(index<0)
            index=0;
       // console.log(`add:i:${index}, p:${value.p}`)
        var val = value;
        var arr = this.inner;
        var len = arr.length;

        for (let i = index; i <= len; i++) {
            let tmp = arr[i];
            arr[i] = val;
            val = tmp;
        }

    }

    static createNode(priority, value) {
        return {p: priority, value: value}
    }

    push(priority, value) {
        var ia = this.inner;
        var length = ia.length;
        if (length == 0) {
            ia[0]=(PriorityQueue.createNode(priority, value));
            return 0;
        }
        var i = Math.floor(length / 2);
        var step = i;
        for (;;) {

            if(!ia[i]){
                console.log(i,ia.length)
            }
            var curr = ia[i].p;
            let halfStep = step / 2;
            step =  (halfStep>0.5?Math.ceil(halfStep):0);
            var d = curr-priority;

            if (d == 0) {
                //exact match in priority, push right here
                this.__add(i, PriorityQueue.createNode(priority, value));
                break;
            }
            else if (d > 0) {
                //priority is lower than q[i], moving cursor left
                i -= step;
            } else if (d < 0) {
                //priority is higher than q[i], moving cursor right
                i += step;
            }

            //max iterations has been made, time to push
            if(step==0){
                this.__add((d>0?i:i+1), PriorityQueue.createNode(priority, value));
                break;
            }
            //to avoid jumping over boundaries
            if(i>=length){
                i = length-1;
            }else
            if(i<0)
            i=0;


        }
    }
}
module.exports = PriorityQueue;

















































































































































































































































































































































































































































































































































































































































































































