/**
 * Created by root on 29.03.16.
 */
/**
 * Created by root on 12.10.15.
 */


var sw = function (syncFunc) {
    if (syncFunc) {
        var start = (new Date()).getTime();
        syncFunc();
        var end = (new Date()).getTime();
        return end - start;
    }
    this.totalElapsed = 0;
}
sw.prototype.start = function () {
    this.end = undefined;
    this.begin = (new Date()).getTime();
}
sw.prototype.stop = function () {
    if (!this.begin)
        return;
    this.end = (new Date()).getTime();
}
sw.prototype.continue = function () {
    if (!this.end)
        return;
    this.totalElapsed += this.getElapsedMs();

    this.start();
}
sw.prototype.getElapsedMs = function () {
    var to = this.end;
    if (!to)
        to = new Date().getTime();
    return this.totalElapsed + to - this.begin;
}

sw.prototype.restart = function () {
    this.totalElapsed = 0;
    this.start();
}
module.exports = sw;