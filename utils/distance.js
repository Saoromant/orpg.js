/**
 * Created by root on 24.03.16.
 */
var abs = Math.abs;
var max = Math.max;
var pow = Math.pow;
var sqrt = Math.sqrt;
// distanceFunction functions
// these return how far away a point is to another

function manhattanDistance(Point, Goal) {	// linear movement - no diagonals - just cardinal directions (NSEW)
    return abs(Point.x - Goal.x) + abs(Point.y - Goal.y);
}

function diagonalDistance(Point, Goal) {	// diagonal movement - assumes diag dist is 1, same as cardinals
    return max(abs(Point.x - Goal.x), abs(Point.y - Goal.y));
}

function euclideanDistance(Point, Goal) {	// diagonals are considered a little farther than cardinal directions
    // diagonal movement using Euclide (AC = sqrt(AB^2 + BC^2))
    // where AB = x2 - x1 and BC = y2 - y1 and AC will be [x3, y3]
    return parseFloat(sqrt(pow(Point.x - Goal.x , 2) + pow(Point.y - Goal.y, 2)));
}

module.exports = { manhattan:manhattanDistance, diagonal:diagonalDistance, euclidean:euclideanDistance };