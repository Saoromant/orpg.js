/**
 * Created by root on 18.03.16.
 */
var intervals = {};
var index = 0;
var interval = function (callback, tm, isPromise) {
    intervals[index] = true;
    var idx = index;
    index++;
    var fn;
    fn = function () {
        if (intervals[idx]) {
            var r = callback();
            if (!isPromise) {
                setTimeout(fn, tm);
            } else {
                r.then(function () {
                        setTimeout(fn, tm)
                    },
                    function (err) {
                        setTimeout(fn, tm);
                        if (x instanceof Error)
                            console.log(`error in safe interval ${err.message} ${err.stack}`);
                        else
                            console.log(`error in safe interval ${JSON.stringify(err)}`);
                    });
            }
        }
    }
    fn();
    return idx;
}
var clr = function (idx) {
    delete intervals[idx];
}
module.exports = {set: interval, clear: clr};
