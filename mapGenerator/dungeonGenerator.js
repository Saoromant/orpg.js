/**
 * Created by root on 24.03.16.
 */
"use strict";
var distanceFunc = require('../utils/distance').manhattan;
var getRandomInt = require('../utils/getRandomArbitrary');
var findPath = require('../pathfinder/pathfinder').findPath;
const DEFAULT_WALL = 1;
/***
 *
 * @param roomNumber max number of rooms to generate
 * @param x width
 * @param y height
 * @returns {*[]} [0] - map, two dim array, that filled with room number, rooms - array of generated rooms
 */
function generateMap(roomNumber, x, y) {
    var map = [];
    var rooms = [];
    for (let i = 0; i < x; i++) {
        map[i] = [];
        for (let j = 0; j < y; j++) {
            map[i][j] = DEFAULT_WALL; //wall
        }
    }

    for (let i = 0; i < roomNumber; i++) {
        var rx = getRandomInt(6, 14);
        var ry = getRandomInt(6, 14);
        generateSquareRoom(rx, ry, rooms);
    }

    for (var i = 0; i < rooms.length; i++) {
        placeRoom(rooms[i], map, 0);
    }


    rooms = rooms.filter((r)=>{return r.placed});
    console.log(`building corridors...`);
    for (var r in rooms) {
        var room = rooms[r];
        var connections = getRandomInt(1, 3);
        if (!room.connected)
            room.connected = [];
        while (connections > 0) {
            var closest = findClosestRoom(room, rooms, room.connected);

            if(!closest) //all rooms already linked
             break;

            room.connected.push(closest);
            if (!closest.connected)
                closest.connected = [];
            closest.connected.push(room);
            var corridor = findPath(
                {x:Math.floor(room.x + room.sizeX / 2),y: Math.floor(room.y + room.sizeY / 2)},
                {x:Math.floor(closest.x + closest.sizeX / 2),y: Math.floor(closest.y + closest.sizeY / 2)},
                map,null,distanceFunc);

            console.log('placing corridor',corridor.length);
            for (var p = 0; p < corridor.length; p++) {
                var pathPoint = corridor[p];
                map[pathPoint.x][pathPoint.y] = 0;
            }
            connections--;
        }
    }

    return [map, rooms];
}


function generateSquareRoom(sizeX, sizeY, rooms) {
    var room = {
        sizeX: sizeX,
        sizeY: sizeY,
        getRandomCoords: function () {
            return [getRandomInt(this.x, this.x + this.sizeX - 1), getRandomInt(this.y, this.y + this.sizeY - 1)]
        }
    };
    room.id = rooms.push(room);

}

function placeRoom(room, map, iter) {
    console.log('trying to place room');
    if (!iter)
        iter = 1;
    else
        iter++;
    var rndX = getRandomInt(1, map.length);
    var rndY = getRandomInt(1, map[0].length);


    var check = true;
    for (let i = rndX - 1; i < (rndX + room.sizeX); i++) {
        var col = map[i];
        if (!col) {
            check = false;
            break;
        }
        for (let j = rndY - 1; j < (rndY + room.sizeY); j++) {
            var point = col[j];
            if (!point || point != DEFAULT_WALL) {
                check = false;
                break;
            }
        }
    }

    if (check) {
        console.log(`placing room ${room.id}`);
        for (var i = rndX; i < (rndX + room.sizeX); i++) {
            for (var j = rndY; j < (rndY + room.sizeY); j++) {
                room.placed = true;
                room.x = rndX;
                room.y = rndY;
                map[i][j] = 0;//room.id;
            }
        }
        return;
    }

    if (iter < 10)
        return;

    placeRoom(room, map, iter);
}

function findClosestRoom(room, rooms, ignore) {

    var closest = null;
    var closest_distance;
    for (var i = 0; i < rooms.length; i++) {
        var check = rooms[i];
        if (check == room  //this room
            || (ignore && ignore.indexOf(check) != -1)) //already linked room
            continue;

        var distance = distanceFunc(check, room);
        if (!closest_distance||distance < closest_distance) {
            closest_distance = distance;
            closest = check;
        }
    }
    return closest;
}

module.exports = {generateMap: generateMap};