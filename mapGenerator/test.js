/**
 * Created by root on 30.03.16.
 */
"use strict";

module.exports = function () {
    var mg = require('./index.js');

    var map = mg.generateDungeon(8,50,50)[0];
    var out = "";
    var gr = {
        clear:'░',
        hard:'▒',
        blocked:'▓',
        path:'x'
    }
    for(let i = 0;i<map.length;i++){
        let row = map[i];
        for(let j=0;j<row.length;j++){
            let  tile = row[j];
            if(tile>0)
             out+=gr.blocked;
            if(tile==0)
             out+=gr.clear;
        }
        out+='\n';
    }
    console.log(out)
}