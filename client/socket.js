/**
 * Created by root on 17.03.16.
 */

function socketClient (host,onmessage) {
    var socket = new WebSocket(`ws://${host}`); //to deploy os add port 8000
    socket.onopen = function() {
        //log something
        renderer.state = 1;
    };

    var handler = onmessage;

    socket.onclose = function (event) {
        if (event.wasClean) {
            console.log('CONNECTION CLOSED');
            renderer.state = 3;

        } else {
            console.warn('CONNECTION BROKEN');
            renderer.state = 2;
        }
        console.log('CODE: ' + event.code + ' reason: ' + event.reason);
    };

    socket.onmessage = function (event) {
        var data = event.data;
        if(handler)
            handler(data);
    };

    socket.onerror = function (error) {
        console.error("ERROR: " + error.message);
    };
    return {
        send:function(data){socket.send(data)},
        onMessage:function(fx){
            handler = fx;
        }
    };
}