/**
 * Created by root on 17.03.16.
 */

    var settings = {
    width:300,
    height:300
};
var stateColor={
    0:"#ff0",
    1:"#0f0",
    2:"#f00",
    3:"#00f"
    };
var renderer = {
    state:0
};
renderer.redraw = function () {
"use strict";
    var ctx = renderer.ctx;
    ctx.clearRect(0,0,settings.width,settings.height);
    var cli = renderer.cli;
    var w = cli.world;
    if(!w) {
        console.log("no world");
        setTimeout(function () {
            requestAnimationFrame(renderer.redraw);
        },500);
        return;
    }


    var visibleObjects = renderer.getObjectsInViewPort();
    //draw map
    for(var x =0;x<visibleObjects.map.length;x++){
        var row = visibleObjects.map[x];
        for(var y = 0;y<row.length;y++){
            var tile = row[y];
            if(tile>0){
                ctx.fillStyle = "#011";
                ctx.fillRect(x*10, y*10, 10, 10);
            }
        }
    }
    //draw objects;
    for(var i = 0;i<visibleObjects.objects.length;i++){
        var obj = visibleObjects.objects[i];
        ctx.fillStyle = obj.color;
        ctx.fillText(obj.id,obj.relX,obj.relY);
        ctx.fillRect(obj.relX, obj.relY, 10, 10);
    }
    ctx.fillStyle  = ctx.strokeStyle = stateColor[renderer.state];
    ctx.beginPath();
    ctx.arc(10, 10,5,0,2*Math.PI);
    ctx.fill();
    ctx.stroke();


    requestAnimationFrame(renderer.redraw);
};

renderer.getObjectsInViewPort = function () {
    var p = renderer.cli.player;
    var cli = renderer.cli;
    var w = cli.world;
    var res = {objects:[],map:[]};
    var x0 = Math.max(0,p.x - settings.width/2);
    var y0 = Math.max(0,p.y - settings.height/2);
    for(var i=0;i<w.objects.length;i++){
        var obj =  w.objects[i];
        obj.relX =obj.x - x0;
        obj.relY = obj.y - y0;
        if(obj.relX>=0&&obj.relX<settings.width
        &&obj.relY>=0&&obj.relY<settings.height)
            res.objects.push(obj);
    }
    res.map = w.map.slice(x0/10,settings.width/10).map((row)=>{ return row.slice(y0/10,settings.height/10) });
    return res;
};

renderer.init = function (cli) {
    renderer.canvas = document.getElementById("scene");
    if(!renderer.canvas)
        throw new Error('oops, no canvas!');

    renderer.cli = cli;
    renderer.ctx = renderer.canvas.getContext("2d");
    renderer.redraw();
}

function getRndInt(){
    "use strict";
   return Math.round(Math.random()*10) ;
}