/**
 * Created by root on 17.03.16.
 */

var keyMappings = {
    87:'N',
    68:'E',
    83:'S',
    65:'W'
}

function Client(){
    "use strict";

}
var controlsListener = null;
window.init = function () {
    var cli = new Client();

    renderer.init(cli);
    var host = window.location.host.split(":")[0];
    var socket = socketClient(host, function (data) {
        console.log(data);
        var pd= JSON.parse(data);
        if(pd.length&&pd.length>0)
            pd.forEach((e)=>{applyChanges(e)});
        else
        applyChanges(pd);

        function applyChanges(d){
            console.log('applying',d);
            "use strict";
            var obj = cli.world.players[d.id];
            if(!obj)
            {
                obj=  {id:d.id, x:d.x,y:d.y, color:d.color};
                console.log('adding new player',obj)
                cli.world.players[d.id] = obj;
                cli.world.objects.push(obj);
            }
            if(d.type!="new_obj") {
                if(d.id!=cli.id) {
                    if (d.x)
                        obj.x += parseInt(d.x);
                    if (d.y)
                        obj.y += parseInt(d.y);
                }
                if (d.color)
                    obj.color = d.color;
            }
        }

    });
    document.addEventListener("keydown",controlsListener = function (e){
        "use strict";
        var key = e.keyCode;
        //console.log(key);
        socket.send(JSON.stringify({ type:"ctrl", data:key,id:cli.id}));
        var p = cli.player;
        var w = cli.world;
        switch (keyMappings[key]){
            case 'N':
                if(p.y>0)p.y -=3;
                break;
            case 'E':
                if(p.x<300)p.x +=3;
                break;
            case 'S':
                if(p.y<300)p.y +=3;
                break;
            case 'W':
                if(p.x>0)p.x -=3;
                break;
            default:
                break;
        }
        // switch (key){

        // }
    });
    sendAJAX("initClient",null, function (r) {
        var data = JSON.parse(r);
        cli.id = data.id;
        cli.world = data.world;
        cli.world.objects=[];
        cli.player = cli.world.players[data.id];
        var ok = Object.keys(cli.world.players);
        for(var i = 0;i<ok.length;i++){
            cli.world.objects.push(cli.world.players[ok[i]])
        }
        if(typeof(localStorage)!="undefined"){
            localStorage.setItem("clientId",cli.id);
        }
    })

}



function sendAJAX(url, data, callback) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", url);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //xmlhttp.setRequestHeader("Origin", "localhost");

    var pseudoCookie = null;
    if(typeof(localStorage)!="undefined"){
        pseudoCookie = localStorage.getItem("clientId");
    }
    if(pseudoCookie)
    xmlhttp.setRequestHeader("P-Cookie", pseudoCookie);
    var stringData = "";
    if(data) {
        var keys = Object.keys(data);
        for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            if (stringData)
                stringData += '&';
            stringData += k + "=" + data[k];
        }
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            // if (xmlhttp.status == 400) {
            console.log(xmlhttp.response);
            if (callback)
                callback(xmlhttp.responseText);
            //     } else {
            //         console.log('error')
            //     }
        }
    }
    xmlhttp.send(stringData);
}


