/**
 * Created by root on 22.03.16.
 */
var http = require('http');
var fs = require('fs');
var Game = require('../core/index.js');


function sendFileResp(path,resp){
    fs.readFile(path,(err,data)=>{
        if(!err) {
            resp.writeHead(200,{"Content-Type":"text/html"});
            resp.write(data);
        }else{
            resp.writeHead(503);
            resp.write("oops, something went wrong");
        }
        resp.end();
    });
}

function parseCookies(request) {
    var list = {},
        rc = request.headers.cookie;
    if(!rc&&request.headers.cookie)
        rc = request.headers.cookie["p-cookie"];
    rc && rc.split(';').forEach(function (cookie) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}
var g = new Game();
var createServer = require("./websocket.js");


g.start(function(data){srv.sendToAll(data)});

function onRequest(req,resp){
    if(req.url.indexOf("lib")!=-1){
        var path = req.url.substring(req.url.indexOf("lib")+3);
        sendFileResp(`./client${path}`,resp);
    }else if(req.url.indexOf("initClient")!=-1){
        var cookie =parseCookies(req);
        var id = cookie["uid"];
        if(!id){
            id = Math.round(Math.random()*1e15).toString(36);
            resp.writeHead(200,{"Set-Cookie":`uid=${id}`})
        }
        var w =  g.initClient(id);
        resp.write(JSON.stringify({id:id,world:w}));
        resp.end();
    }
    else{
        sendFileResp("./client/client.html",resp);
    }
}
var server = http.createServer(onRequest);
var srv = createServer(server,(data,sendToAll)=>{
    var d = g.proceedUserInput(data);
});

module.exports = server;



