/**
 * Created by root on 17.03.16.
 */

function createServer(server,recieveData) {


    var WebSocketServer = require('ws').Server
        , wss = new WebSocketServer({server: server});


    function sendToAll(da) {
        wss.clients.forEach(function each(client) {
            client.send(JSON.stringify(da));
        });
    }
    wss.on('connection', function connection(ws) {
        ws.on('message', function incoming(message) {
            //console.log('received: %s', message);
            recieveData(message, function (da) {
                sendToAll(da);

            });
        });
    });
    return {server:wss,sendToAll:sendToAll  };
}

module.exports =  createServer;