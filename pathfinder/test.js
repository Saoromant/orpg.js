/**
 * Created by root on 30.03.16.
 */
"use strict";
module.exports = function () {
    var pathfinder = require('./pathfinder');
    var getRandomInt = require('../utils/getRandomArbitrary');
    var Stopwatch = require('../utils/stopwatch');
    var Point = function (x,y) {
        this.x = x;
        this.y = y;
    };
    var map = [];

    for(let i = 0;i<50;i++){
        map[i] = [];
        for(let j=0;j<50;j++){
            map[i][j] = getRandomInt(0,3);
        }
    }

    var sw = new Stopwatch();
    sw.start();
    var path = pathfinder.findPath(new Point(0,0),new Point(map.length-1,map.length-1),map,null);
    console.log(sw.getElapsedMs())
    var output = "";

    var outputPath = "\n";
    var gr = {
        clear:'░',
        hard:'▒',
        blocked:'▓',
        path:'x'
    }

    for(let i  = 0;i<map.length;i++){
        outputPath += "\n";
        output += "\n";
        for(let j=0;j<map.length;j++){
            var t =  map[i][j];
            var mt =  (t>1?gr.blocked:(t>0?gr.hard:gr.clear));
            if(path.filter((p)=>{ return (p.x == i && p.y == j); }).length>0){
                outputPath += t//gr.path;
            }else
            {
                outputPath += mt;
            }

            output +=mt;

        }
    }

    console.log(`${output}||${outputPath}`);

};