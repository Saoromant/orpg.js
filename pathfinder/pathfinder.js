/**
 * Created by root on 24.03.16.
 */
"use strict";
var distance = require('../utils/distance').euclidean;

var a = {};
class Node {
    constructor(arg0, arg1, arg2,arg3) {
        var grid;
        if (typeof arg2 == "object" && arg2 instanceof Array) {
            grid = arg2
        } else if (typeof arg1 == "object" && arg1 instanceof Array) {
            grid = arg1;
        }
        else {
            throw new Error("Invalid constructor arguments, no path grid");
        }
        if (typeof arg0 == "object") {
            if (arg0 instanceof Array && arg0.length > 1) {
                this.x = arg0[0];
                this.y = arg0[1];
            } else {
                this.x = arg0.x;
                this.y = arg0.y;
            }
        } else {
            this.x = arg0;
            this.y = arg1;
        }

        if (isNaN(this.x) || isNaN(this.y))
            throw new Error(`Invalid node constructor args x:${this.x} y:${this.y}`);

        this.cost = grid[this.x][this.y];
        if (isNaN(this.cost))
            throw new Error(`Path cost is ${this.cost} at ${this.x}, ${this.y}`);

     /*   if(arg2 instanceof Node){
            this.parent = arg2;
        }else
        if(arg3 instanceof  Node){
            this.parent = arg3;
        }
*/
        this.gScore = 0;
        this.hash = `${this.x}${this.y}`;
    }

    static getNodeNeighbors(node, grid){
           var res = [];

        if(!(node instanceof Node))
            throw new Error(`node param must be instance of Node class`);

        for(let i=-1;i<2;i++){
            let x = node.x +i;
            if(x<0||x>=grid.length)
                continue;
            for(let j=-1;j<2;j++){
                let y = node.y + j;
                if(y<0||y>=grid.length||(i==0&&j==0))
                continue;
                res.push({x:x,y:y});
            }
        }
    return res;
    }
}

/***
 *
 * @param start Object, must define x,y coords of starting node.
 * @param goal Object, must define x,y of goal point
 * @param grid Array of arrays, [x][y], value is path cost
 * @param maxPassableCost inclusive boundary for passable nodes, cells with cost greater than maxPassableCost will be treated as walls
 * @returns Array of points of path, if empty - there is no path
 */
a.findPath = function (start, goal, grid,maxPassableCost,distanceFunc) {

    distance = distanceFunc||distance;
    if(goal instanceof  Array){
        goal = {x:goal[0],y:goal[1]}
    }
    var path = [];
    var h = function (x) {
        return distance(x,goal);
    };
    var g = function (x) {
        return  x.cost + distance(x,start) ;
    };
    var f = function (x) {
        return h(x)+g(x);
    };
    var open = [new Node(start, grid)];

    console.log(`begin pathing from ${open[0].x} ${open[0].y} to ${goal.x} ${goal.y} aprox distance:${distance(open[0],goal)}`);
    open[0].gScore = g(open[0]);
    open[0].fScore = h(open[0])+open[0].gScore;
    var closed = {};
    var openSet = {};
    while (open.length > 0) {

        open.sort((a,b)=>{openSet[a.hash] = 1; return a.fScore<b.fScore});
        var current = open.pop();
        delete openSet[current.hash];
        closed[current.hash] = 1;

        if(current.x == goal.x && current.y==goal.y){
            //we reached our goal
            console.log('goal');
            do{
                path.push(current);
                current = current.parent;
            }while (current.parent);
            return path;
        }

       var neighbors  =  Node.getNodeNeighbors(current,grid);
        for(var n of neighbors){
            var newNode = new Node(n,grid,current);
            if(maxPassableCost&&newNode.cost>maxPassableCost){
                closed[newNode.hash] = 1;
                continue;
            }
            let tentativeScore = g(newNode) + current.cost;
            if(closed[newNode.hash])
                continue;
            if(!openSet[newNode.hash])
            {
                openSet[newNode.hash] = 1;
                open.push(newNode);
            }else if(newNode.gScore>current.gScore){
                console.log('skipped node');
                continue;
            }
            newNode.gScore = tentativeScore;
            newNode.parent = current;
            newNode.fScore = h(newNode) + newNode.gScore;
        }

    }

    return path;

}

module.exports = a;