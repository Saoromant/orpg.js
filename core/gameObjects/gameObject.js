/**
 * Created by root on 18.03.16.
 */
"use strict";
class GameObject {
    constructor(id, x, y) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.color = "#000";
        this.wasIntersected = false;
    }

    resetToDefaultState(changeset){
        var self = this;
        if(!this.wasIntersected&&this.color!="#000"){
            changeset.push({color:"#000",id:self.id});
        }
        this.wasIntersected = false;
    }

    hasIntersection(x, y) {
        this.wasIntersected = true;
        return (Math.abs(this.x - x) < 10 && Math.abs(this.y - y) < 10);
    }

    interact(obj, changeset,callerIsObj) {
        var hasDiff = false;
        var diff = {};
        diff.id = this.id;
        if (this.hasIntersection(obj.x, obj.y)) {

            if (this.color != "#f00") {
                this.color = "#f00";
                diff.color = "#f00";
                hasDiff = 1;
            }

        } else if(!this.wasIntersected){
            if (this.color != "#000") {
                this.color = "#000";
                diff.color = "#000";
                hasDiff = 1;

            }
        }
        if (hasDiff) {
            changeset.push(diff);
        }

        if(!callerIsObj)
            obj.interact(this,changeset,true);
    }


}

module.exports = GameObject;