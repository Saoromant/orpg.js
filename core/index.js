/**
 * Created by root on 17.03.16.
 */
var World = require('./world');
var Processor = require('./processor/processor');
var getRandomArbitrary = require('../utils/getRandomArbitrary')
var keyMappings = {
    87:'N',
    68:'E',
    83:'S',
    65:'W'
}
var g = function () {
    "use strict";
    var w = this.world = new World(1000,1000);

};
g.prototype.start = function (broadcast) {
    this.proc = new Processor(this.world,broadcast);
    this.proc.start();
}
g.prototype.proceedUserInput = function (data) {
    var input = JSON.parse(data);
    if (input.type == "ctrl") {
    //    console.log(input.data);
        var w=  this.world;
        var p = w.players[input.id];
        if(!p)
            return;
        var d = {x:0,y:0, id:data.id};
        switch (keyMappings[input.data]){
            case 'N':
                if(p.y>0)d.y -=3;
                break;
            case 'E':
                if(p.x<w.maxX)d.x +=3;
                break;
            case 'S':
                if(p.y<w.maxY)d.y +=3;
                break;
            case 'W':
                if(p.x>0)d.x -=3;
                break;
            default:
                break;
        }
if(keyMappings[input.data]) {
    d.id = p.id;
    d.type = "mov";
    if(!this.proc.pendingChanges[p.id])
        this.proc.pendingChanges[p.id] = [];
    this.proc.pendingChanges[p.id].push(d);
    return d;
}
    }
};
g.prototype.initClient = function (id) {
    var player = this.world.getObject(id);
    if(!player) {
        player = this.world.addObject({
            id: id,
            y: getRandomArbitrary(0,this.world.maxX),
            x: getRandomArbitrary(0,this.world.maxY)});

    }
    var plrs = this.world.players;
    if(!this.proc.pendingChanges[id])
        this.proc.pendingChanges[id] = [];
    this.proc.pendingChanges[id].push({type:'new_obj',id:id,x:player.x,y:player.y});
    return {players:plrs,map:this.world.map};
};

module.exports = g;