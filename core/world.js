/**
 * Created by root on 18.03.16.
 */
"use strict";

var GameObject = require('./gameObjects/gameObject');
var mapGenerator = require('../mapGenerator/index.js');
var World = function (sizeX,sizeY) {
    this.objects = [];
    this.players = {};
    this.maxX = sizeX;
    this.maxY = sizeY;
    //map scale 1 tile to 10 pixels;
    this.map = mapGenerator.generateDungeon(10,sizeX/10,sizeY/10)[0];
}

World.prototype.addObject = function (o) {
    var obj = new GameObject(o.id,o.x,o.y);
    this.objects.push(obj);
    this.players[obj.id] = obj;
    return obj;
}
World.prototype.getObject=function(id){
    return this.players[id];
}

module.exports = World;