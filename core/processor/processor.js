/**
 * Created by root on 18.03.16.
 */
"use strict";
    var safeInterval = require('../../utils/safeInterval.js');
var proc = function (world, broadcast) {
    this.world = world;
    this.broadcast = broadcast;
    this.pendingChanges = {};

}

proc.prototype.process = function () {
    var w = this.world;
    var o = w.objects;
    var changeset = [];
    var keys  = Object.keys(this.pendingChanges);
    for(let id of keys){
        var p =  w.players[id];
        var change = {id:id,x:0,y:0, type:'mov'};
        let hasMove = 0;
        for(let ch of this.pendingChanges[id]){
            if(p&&ch.type=="mov") {
                hasMove = 1;
                p.x+=ch.x;
                p.y+=ch.y;
                change.x += ch.x;
                change.y += ch.y;
            }
            else
            changeset.push(ch);
        }
        if(hasMove){

            changeset.push(change);}
    }
 /*   for(let ch of this.pendingChanges){
       var p = w.players[ch.id];

        if(p&&ch.type=="mov") {
            p.x += ch.x;
            p.y += ch.y;
        }

        changeset.push(ch);
    }*/
    this.pendingChanges = {};
    for(let i=0;i<o.length-1;i++){
        var obj1 = o[i];
        for(let j=i+1;j<o.length;j++){

            var obj2 = o[j];
            obj1.interact(obj2,changeset);
        }
    }
 if(changeset.length>0) {
     this.broadcast(changeset);
 }
}


proc.prototype.start = function () {
    var self = this;
    safeInterval.set(function () {
        self.process();
    },50,false);
}

module.exports = proc;